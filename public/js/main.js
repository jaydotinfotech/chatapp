const chatform = document.getElementById("chat-form");
const chatmessages = document.querySelector(".chat-messages");
const roomName = document.getElementById('room-name');
const usersName = document.getElementById('users');

const { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

const socket = io();

socket.emit("joinroom", { username, room });

socket.on('roomusers',({room,users}) => {
    roomname(room);
    usersname(users);
})

socket.on("msg", (msg) => {
  outputmsg(msg);

  chatmessages.scrollTop = chatmessages.scrollHeight;
});

chatform.addEventListener("submit", (e) => {
  e.preventDefault(); //page reload na thay

  var msg = document.getElementById("msg").value;

  socket.emit("chatmsg", msg);

  document.getElementById("msg").value = "";
  document.getElementById("msg").focus();
});

function outputmsg(msg) {
  const div = document.createElement("div");
  div.classList.add("message");
  div.innerHTML = `<p class="meta">${msg.username} <span>${msg.time}</span></p>
    <p class="text">
        ${msg.text}
    </p>`;
  document.querySelector(".chat-messages").appendChild(div);
}

function roomname(room){
    roomName.innerText = room
}

function usersname(users){
    usersName.innerHTML = '';
    users.forEach(user=>{
        const li = document.createElement('li');
        li.innerText = user.username;
        userList.appendChild(li);
      });
}
