require("dotenv").config();
const express = require("express");
const path = require("path");
const http = require("http");
const socketio = require("socket.io");
const msgformate = require("./msg");
const {
  userjoinroom,
  getcurrentuser,
  userLeaveChat,
  getRoomUsers,
} = require("./user");
const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(express.static(path.join(__dirname, "public")));
const PORT = process.env.PORT;
const botname = "BOT";

//RUN WHEN NEW USER CONNECT
io.on("connection", (socket) => {
  socket.on("joinroom", ({ username, room }) => {
    const user = userjoinroom(socket.id, username, room);

    socket.join(user.room);
    // WELCOME MSG WHEN USER CONNECT
    socket.emit("msg", msgformate(botname, `WELCOME ${user.username}`));

    socket.broadcast
      .to(user.room)
      .emit("msg", msgformate(botname, `${user.username} join`));

    io.to(user.room).emit('roomusers',{
      room:user.room,
      users: getRoomUsers(user.room)
    })  
  });

  socket.on("chatmsg", (msg) => {
    const user = getcurrentuser(socket.id);

    io.to(user.room).emit("msg", msgformate(user.username, msg));
  });

  socket.on("disconnect", () => {
    const user = userLeaveChat(socket.id);
    console.log('user disconnected')
    // console.log(user)
    if (user) {
      io.to(user.room).emit(
        "msg",
        msgformate(botname, `${user.username} left`)
      );

  //     io.to(user.room).emit('roomusers',{
  //       room:user.room,
  //       users:getRoomUsers(user.room)
  //  })
    }
  });
});

server.listen(PORT, () => {
  console.log(`server is running on ${PORT}`);
});